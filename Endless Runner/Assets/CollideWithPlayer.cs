using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollideWithPlayer : MonoBehaviour
{
    Animator animator;

    private void Start()
    {
        animator = GetComponentInParent<Animator>();
    }
    private void OnTriggerEnter(Collider collision)
    {
       
        if (collision.gameObject.CompareTag("Enemy"))
        {
            GameManager.gameOver = true;
            animator.Play("GameOver");
        }
    }
}
