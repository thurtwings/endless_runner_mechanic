using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour
{
    public GameObject[] tiles;
    public float zSpawn = 0;
    public float tileLenght = 30;
    public int numberOfTiles = 5;
    public Transform player;

    public List<GameObject> activesTiles = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < numberOfTiles; i++)
        {
            if (i == 0)
                SpawnTile(1);
            else
                SpawnTile(Random.Range(0, tiles.Length));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (player.position.z - 35f > zSpawn - (numberOfTiles * tileLenght))
        {
            SpawnTile(0);
            RemoveTile();
        }
    }

    void RemoveTile()
    {
        Destroy(activesTiles[0]);
        activesTiles.RemoveAt(0);
    }

    public void SpawnTile(int index)
    {
        GameObject nt = Instantiate(tiles[index], transform.forward * zSpawn, transform.rotation) as GameObject;
        activesTiles.Add(nt);
        zSpawn += tileLenght;
    }
}
