// GENERATED AUTOMATICALLY FROM 'Assets/Script/WolfEndlessRunner.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @WolfEndlessRunner : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @WolfEndlessRunner()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""WolfEndlessRunner"",
    ""maps"": [
        {
            ""name"": ""Wolfy"",
            ""id"": ""509f5a52-e97e-4f81-beda-a5c0c57b5c05"",
            ""actions"": [
                {
                    ""name"": ""Left"",
                    ""type"": ""Button"",
                    ""id"": ""fbadcce7-1680-4885-b7ff-45112becb3a8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Right"",
                    ""type"": ""Button"",
                    ""id"": ""cd22cbb2-a8e4-4319-8258-d3abb2ada447"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""1fed70ad-f63e-4f4e-8d88-f6643bb4d960"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Crawl"",
                    ""type"": ""Button"",
                    ""id"": ""c9e9d564-9bf9-415d-b49d-130aabd7fdfa"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""f77b556d-2772-4fc6-a203-1e01f91e7d15"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8153bc2a-b0b8-426f-8523-c3cca08fa01f"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Left"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c7cf5970-6e9e-46d2-aceb-123b71c6782f"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3181f978-c187-4859-ad23-156464f376a5"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Right"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0df341f8-bf49-4766-9216-2178e33c3b9b"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dd30a4aa-2662-4bdf-9805-789b8bcebee3"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""346b1206-25fc-4457-868b-8500d44e18ee"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Crawl"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""95ed4e2f-d9aa-4660-888e-67f83d9ad97c"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Crawl"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""UI"",
            ""id"": ""938da753-4833-4393-b589-5dad0cf0977d"",
            ""actions"": [
                {
                    ""name"": ""Select"",
                    ""type"": ""Button"",
                    ""id"": ""90c9321b-f753-46e5-9ff9-328da8306a37"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""bee08bf2-9a63-4d4e-b85a-5459a2a0161c"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Select"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0cd04f72-3cd9-436c-acd0-87c605e1cc11"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Select"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""878061db-5ad9-4932-b88e-0a99f240524e"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Select"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Wolfy
        m_Wolfy = asset.FindActionMap("Wolfy", throwIfNotFound: true);
        m_Wolfy_Left = m_Wolfy.FindAction("Left", throwIfNotFound: true);
        m_Wolfy_Right = m_Wolfy.FindAction("Right", throwIfNotFound: true);
        m_Wolfy_Jump = m_Wolfy.FindAction("Jump", throwIfNotFound: true);
        m_Wolfy_Crawl = m_Wolfy.FindAction("Crawl", throwIfNotFound: true);
        // UI
        m_UI = asset.FindActionMap("UI", throwIfNotFound: true);
        m_UI_Select = m_UI.FindAction("Select", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Wolfy
    private readonly InputActionMap m_Wolfy;
    private IWolfyActions m_WolfyActionsCallbackInterface;
    private readonly InputAction m_Wolfy_Left;
    private readonly InputAction m_Wolfy_Right;
    private readonly InputAction m_Wolfy_Jump;
    private readonly InputAction m_Wolfy_Crawl;
    public struct WolfyActions
    {
        private @WolfEndlessRunner m_Wrapper;
        public WolfyActions(@WolfEndlessRunner wrapper) { m_Wrapper = wrapper; }
        public InputAction @Left => m_Wrapper.m_Wolfy_Left;
        public InputAction @Right => m_Wrapper.m_Wolfy_Right;
        public InputAction @Jump => m_Wrapper.m_Wolfy_Jump;
        public InputAction @Crawl => m_Wrapper.m_Wolfy_Crawl;
        public InputActionMap Get() { return m_Wrapper.m_Wolfy; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(WolfyActions set) { return set.Get(); }
        public void SetCallbacks(IWolfyActions instance)
        {
            if (m_Wrapper.m_WolfyActionsCallbackInterface != null)
            {
                @Left.started -= m_Wrapper.m_WolfyActionsCallbackInterface.OnLeft;
                @Left.performed -= m_Wrapper.m_WolfyActionsCallbackInterface.OnLeft;
                @Left.canceled -= m_Wrapper.m_WolfyActionsCallbackInterface.OnLeft;
                @Right.started -= m_Wrapper.m_WolfyActionsCallbackInterface.OnRight;
                @Right.performed -= m_Wrapper.m_WolfyActionsCallbackInterface.OnRight;
                @Right.canceled -= m_Wrapper.m_WolfyActionsCallbackInterface.OnRight;
                @Jump.started -= m_Wrapper.m_WolfyActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_WolfyActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_WolfyActionsCallbackInterface.OnJump;
                @Crawl.started -= m_Wrapper.m_WolfyActionsCallbackInterface.OnCrawl;
                @Crawl.performed -= m_Wrapper.m_WolfyActionsCallbackInterface.OnCrawl;
                @Crawl.canceled -= m_Wrapper.m_WolfyActionsCallbackInterface.OnCrawl;
            }
            m_Wrapper.m_WolfyActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Left.started += instance.OnLeft;
                @Left.performed += instance.OnLeft;
                @Left.canceled += instance.OnLeft;
                @Right.started += instance.OnRight;
                @Right.performed += instance.OnRight;
                @Right.canceled += instance.OnRight;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Crawl.started += instance.OnCrawl;
                @Crawl.performed += instance.OnCrawl;
                @Crawl.canceled += instance.OnCrawl;
            }
        }
    }
    public WolfyActions @Wolfy => new WolfyActions(this);

    // UI
    private readonly InputActionMap m_UI;
    private IUIActions m_UIActionsCallbackInterface;
    private readonly InputAction m_UI_Select;
    public struct UIActions
    {
        private @WolfEndlessRunner m_Wrapper;
        public UIActions(@WolfEndlessRunner wrapper) { m_Wrapper = wrapper; }
        public InputAction @Select => m_Wrapper.m_UI_Select;
        public InputActionMap Get() { return m_Wrapper.m_UI; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(UIActions set) { return set.Get(); }
        public void SetCallbacks(IUIActions instance)
        {
            if (m_Wrapper.m_UIActionsCallbackInterface != null)
            {
                @Select.started -= m_Wrapper.m_UIActionsCallbackInterface.OnSelect;
                @Select.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnSelect;
                @Select.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnSelect;
            }
            m_Wrapper.m_UIActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Select.started += instance.OnSelect;
                @Select.performed += instance.OnSelect;
                @Select.canceled += instance.OnSelect;
            }
        }
    }
    public UIActions @UI => new UIActions(this);
    public interface IWolfyActions
    {
        void OnLeft(InputAction.CallbackContext context);
        void OnRight(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnCrawl(InputAction.CallbackContext context);
    }
    public interface IUIActions
    {
        void OnSelect(InputAction.CallbackContext context);
    }
}
