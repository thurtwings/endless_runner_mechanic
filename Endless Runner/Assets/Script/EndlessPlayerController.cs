using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndlessPlayerController : MonoBehaviour
{

    Animator animator;
    Rigidbody rb;
    float currentVelocity;
    public float jumpForce;
    public float startSpeed = 0f;
    public float maxSpeed = 10f;
    public float speedMultiplier = .1f;
    float currentSpeed;
    WolfEndlessRunner inputActions;
    public bool hitLeft, hitRight, hitJump, hitCrawl;


    public bool isNextLeftEdge;
    public bool isNextRightEdge;
    private int desiredLine = 2; //0: Left left / 1: left middle / 2: Middle / 3: right middle / 4: right right
    public float laneDistance = 2; //distance between two lanes



   
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        
       
    }


    private void OnEnable()
    {
        if (inputActions == null)
        {
            inputActions = new WolfEndlessRunner();

            inputActions.Wolfy.Left.performed += i => hitLeft = true;
            inputActions.Wolfy.Left.canceled += i => hitLeft = false;

            inputActions.Wolfy.Right.performed += i => hitRight = true;
            inputActions.Wolfy.Right.canceled += i => hitRight = false;

            inputActions.Wolfy.Jump.performed += i => hitJump = true;
            inputActions.Wolfy.Jump.canceled += i => hitJump = false;

            inputActions.Wolfy.Crawl.performed += i => hitCrawl = true;
            inputActions.Wolfy.Crawl.canceled += i => hitCrawl = false;
        }

        inputActions.Enable();
    }

    private void OnDisable()
    {
        inputActions.Disable();
    }
    // Update is called once per frame
    void Update()
    {
        float delta = Time.deltaTime;
        if (!GameManager.gameOver)
        {
            AvoidFallingFromTheEdge();
            TickInput(delta);
        }
        else currentSpeed = 0;
            
        
    }

    
    public void TickInput(float delta)
    {
        HandleForwardMovement(delta);

        if (hitLeft && !isNextLeftEdge)
            HandleLeftInput();

        if (hitRight && !isNextRightEdge)
            HandleRightInput();

        if (hitJump)
            HandleJumpInput();

        
        HandleCrawlInput(hitCrawl);



    }

    void AvoidFallingFromTheEdge()
    {
        if (desiredLine == 0) isNextLeftEdge = true;
        else isNextLeftEdge = false;

        if (desiredLine == 4) isNextRightEdge = true;
        else isNextRightEdge = false;
            
        
    }
    void HandleForwardMovement(float delta)
    {
        
        if (currentSpeed < maxSpeed)
            currentSpeed +=speedMultiplier* delta;
        else
            currentSpeed = maxSpeed;

        animator.speed = currentSpeed;
        if (!hitCrawl)
        {
            //animator.SetBool("crawl", hitCrawl);
            animator.SetFloat("Vertical", currentSpeed); 
        }
        //else HandleCrawlInput();
    }
    void HandleLeftInput()
    {
        
        desiredLine--;
        if (desiredLine == -1)
        {
            desiredLine = 0;
        }
        
        Vector3 targetPosition = transform.position += (Vector3.left * laneDistance);

        transform.position = targetPosition;

        hitLeft = false;
    }
    void HandleRightInput()
    {
        desiredLine++;
        if (desiredLine == 5)
            desiredLine = 4;
        

        Vector3 targetPosition = transform.position += (Vector3.right * laneDistance);

        transform.position = targetPosition;

        hitRight = false;
    }
    void HandleJumpInput()
    {
        animator.Play("Jump");
        
        Debug.Log("Jump Performed");
    }
    void HandleCrawlInput(bool hitCrawl)
    {
        
        animator.SetBool("crawl", hitCrawl);
        
    }
}
