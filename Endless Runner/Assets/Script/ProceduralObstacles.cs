using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralObstacles : MonoBehaviour
{

    public GameObject[] obstacles;
    int random;
    bool isActive;
    // Start is called before the first frame update
    void Start()
    {
        
        SpawnObtacles();
    }

    void SpawnObtacles()
    {
        foreach (Transform item in transform)
        {
            foreach (Transform obstalcePosition in item)
            {
                
                GameObject obstacle = Instantiate(obstacles[Random.Range(0, obstacles.Length )], obstalcePosition.transform.position, Quaternion.identity, obstalcePosition) as GameObject;
                obstacle.SetActive(IsActive());

            }
        }
    }


    bool IsActive()
    {
        random = Random.Range(0, 2);
        if (random == 0) return false;
        else return true;
    }
}
